# 版本控制系统
git是什么呢？git是目前世界上最牛逼的分布式版本控制系统。那什么是版本控制系统？版本控制是一种记录一个或若干文件内容变化，以便将来查阅特定版本修订情况的系统。想象一下，你要写一篇长篇大论，从开始写到完工会经历无数次的增删改，你想删除或改动一段内容，但是有可能将来会用到被删除的内容，这时候你会怎么做？新建一个word文档，将内容存为两份，一份存储修改之前的内容，一份存储修改之后的，听起来是个不错的办法。但是每次改动都这样做的话，会导致你的电脑里存了很多份文档，最后找起来也很麻烦。这时候一个版本控制系统的作用就凸显出来了。版本控制系统可以分为三类，本地版本控制系统、集中式版本控制系统、以及分布式版本控制系统。

## 本地版本控制系统
许多人习惯用复制整个项目目录的方式来保存不同的版本，或许还会改名加上备份时间以示区别。这么做唯一的好处就是简单。不过坏处也不少：有时候会混淆所在的工作目录，一旦弄错文件丢了数据就没法撤销恢复。

为了解决这个问题，人们很久以前就开发了许多种本地版本控制系统，大多都是采用某种简单的数据库来记录文件的历次更新差异
## 集中式版本控制系统
集中式版本控制系统，版本库是集中存放在中央服务器的，而干活的时候，用的都是自己的电脑，所以要先从中央服务器取得最新的版本，然后开始干活，干完活了，再把自己的活推送给中央服务器。中央服务器就好比是一个图书馆，你要改一本书，必须先从图书馆借出来，然后回到家自己改，改完了，再放回图书馆。

集中式版本控制系统最大的毛病就是必须联网才能工作，如果在局域网内还好，带宽够大，速度够快，可如果在互联网上，遇到网速慢的话，可能提交一个10M的文件就需要5分钟，这还不得把人给憋死啊。
## 分布式版本控制系统
分布式版本控制系统根本没有“中央服务器”，每个人的电脑上都是一个完整的版本库，这样，你工作的时候，就不需要联网了，因为版本库就在你自己的电脑上。既然每个人电脑上都有一个完整的版本库，那多个人如何协作呢？比方说你在自己电脑上改了文件A，你的同事也在他的电脑上改了文件A，这时，你们俩之间只需把各自的修改推送给对方，就可以互相看到对方的修改了。

和集中式版本控制系统相比，分布式版本控制系统的安全性要高很多，因为每个人电脑里都有完整的版本库，某一个人的电脑坏掉了不要紧，随便从其他人那里复制一个就可以了。而集中式版本控制系统的中央服务器要是出了问题，所有人都没法干活了。

在实际使用分布式版本控制系统的时候，其实很少在两人之间的电脑上推送版本库的修改，因为可能你们俩不在一个局域网内，两台电脑互相访问不了，也可能今天你的同事病了，他的电脑压根没有开机。因此，分布式版本控制系统通常也有一台充当“中央服务器”的电脑，但这个服务器的作用仅仅是用来方便“交换”大家的修改，没有它大家也一样干活，只是交换修改不方便而已。

# Git安装
git是目前最流行的分布式版本控制系统。Git目前可以在Windows、Mac、Linux三大平台上运行。可以通过[<font color = red>download</font>](https://git-scm.com/downloads)下载git并安装

在Linux下也可以通过以下命令安装
```bash
$ sudo apt-get install git
```
安装完成后输入git命令，看看系统是否已经成功安装
```bash
$ git
The program 'git' is currently not installed. You can install it by typing:
sudo apt-get install git
```
出现上面的提示说明系统没有安装git，安装成功会出现以下提示
![git安装成功](Images/git%E5%AE%89%E8%A3%85%E6%88%90%E5%8A%9F.png)

# Git配置
安装完成后，需要先配置下自己的Git工作环境。配置工作只需一次，以后升级时还会沿用现在的配置。当然，如果需要，你随时可以用相同的命令修改已有的配置。

Git提供了一个叫做`git config`的工具，专门用来配置或读取相应的工作环境变量。而正是由这些环境变量，决定了Git在各个环节的具体工作方式和行为。Git有三种环境变量，分别以文件的形式存放在三个不同的地方。
1. **系统变量**
Linux系统中，系统配置文件位于`/etc/gitconfig`
Windows系统中，系统配置文件位于`%git安装目录%\mingw64\etc\gitconfig`
该文件包含系统上每一个用户及他们仓库的通用配置。使用`git config --system`命令配置时，它会从此文件读写配置变量。系统变量对所有用户都适用。
2. **用户变量**
Linux系统中，用户配置文件位于`~/.gitconfig`或`~/.config/git/config`
Windows系统中，用户配置文件位于`C:\Users\%username%\.gitconfig`
用户变量只针对当前用户。 使用`git config --global`命令配置时，它会从此文件读写配置用户变量。
3. **本地仓库变量**
当前仓库的git目录中的配置文件（也就是仓库目录中的`.git/config`文件），这里的配置仅仅针对当前项目有效。使用`git config --local`命令配置时，会从此文件读写配置变量，但必须在当前仓库目录下键入命令。
<font color = red>每一个级别的配置都会覆盖上层的相同配置，所以`.git/config`里的配置会覆盖`/etc/gitconfig`中的同名变量。</font>

## 用户信息配置
第一个要配置的是你个人的用户名称和电子邮件地址。这两条配置很重要，每次 Git 提交时都会引用这两条信息，说明是谁提交了更新，所以会随更新内容一起被永久纳入历史记录：
```bash
$ git config --global user.name "qrs101"
$ git config --global user.email qrs101@outlook.com
```
如果用了`--global`选项，那么更改的配置文件就是位于你用户主目录下的那个，以后你所有的项目都会默认使用这里配置的用户信息。如果要在某个特定的项目中使用其他名字或者电邮，只要去掉`--global`选项重新配置即可，新的设定保存在当前项目的`.git/config`文件里。

常用的配置主要就是用户名和邮箱，在Linux下一般也会配置一下文本编辑器（用不惯vim的话），当然还有其他配置，这里就细说了。
## 查看配置信息
要检查已有的配置信息，可以使用`git config --list`也可以简写为`git config -l`命令：
```bash
$ git config -l
core.symlinks=false
core.autocrlf=true
core.fscache=true
color.diff=auto
color.status=auto
color.branch=auto
color.interactive=true
help.format=html
rebase.autosquash=true
http.sslcainfo=D:/ProgramFiles/Git/mingw64/ssl/certs/ca-bundle.crt
http.sslbackend=openssl
diff.astextplain.textconv=astextplain
filter.lfs.clean=git-lfs clean -- %f
filter.lfs.smudge=git-lfs smudge -- %f
filter.lfs.required=true
filter.lfs.process=git-lfs filter-process
credential.helper=manager
user.name=qrs101
filter.lfs.clean=git-lfs clean -- %f
filter.lfs.smudge=git-lfs smudge -- %f
filter.lfs.process=git-lfs filter-process
filter.lfs.required=true
user.name=qrs101
user.email=qrs101@outlook.com
core.repositoryformatversion=0
core.filemode=false
core.bare=false
core.logallrefupdates=true
core.symlinks=false
core.ignorecase=true
remote.origin.url=git@git.coding.net:qrs101/Uploads.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
```
有时候会看到重复的变量名，那就说明它们来自不同的配置文件（比如 /etc/gitconfig 和 ~/.gitconfig），不过最终 Git 实际采用的是最后一个。

也可以直接查看某个环境变量的配置，只需要使用不同的选项
```bash
$ git config [--system][--global][--local] --list
```
查看不同环境变量如下图
![git查看环境变量](Images/git%E6%9F%A5%E7%9C%8B%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F.png)


# 创建git仓库
git仓库（repository）就是一个目录，这个目录下的所有文件都可以被git管理起来，每个文件的修改、删除，git都能跟踪。

进入你想要管理的目录，输入以下命令创建一个git仓库
```Bash
$ pwd
D:\data\git

$ git init
Initialized empty Git repository in D:/data/git/.git/
```
`pwd`命令查看当前目录，`git init`命令初始化当前目录为git仓库。创建好后，该目录下多了个隐藏的.git文件夹，这个是用来跟踪管理git仓库的，不要随意修改这个文件夹。当然也不一定非要在空目录下创建git仓库，在有其他文件的目录下也可以创建git仓库。

# git仓库的文件管理
## 跟踪并提交文件
有了git仓库后，现在可以跟踪仓库里的文件并提交。例如在之前创建的git仓库中，新建一个test.txt文件，在文件里输入"This is a test file"并保存。接下来把这个文件提交给git仓库。首先用`git add`命令把文件添加到git仓库
```bash
git add test.txt
```
执行后不会有任何提示，说明添加成功。然后用`git commit`命令把文件提交给git仓库
```bash
git commit -m "create test.txt"
```
`-m`参数后面输入的是本次提交的说明，可以输入任何内容，当然最好是能够表明这次提交作用的内容。用两天命令来提交是因为git可以一次提交很多文件，比如你修改或添加了好几个文件，然后对每个文件都`git add`一下，再进行提交。当然，修改的文件比较多的时候，可以直接用
```bash
git add --all
```
命令来一次性添加所有修改的文件到git仓库，再提交。
## 查看当前仓库的状态
工作目录下的每一个每一个文件都不外乎有两种状态：已跟踪和未跟踪。已跟踪的文件是指那些被纳入了版本控制的文件，就是前面说的已经提交给git仓库的文件，未跟踪的文件是那些修改过但还没有提交给git仓库的文件。要查看哪些文件处于什么状态，可以使用`git status`命令
```bash
$ git status
On branch master
nothing to commit, working tree clean
```
这说明当前工作目录相当干净，也就是，所有已跟踪文件在上次提交后都并无修改。此外还说明当前目录下没有出现任何处于未跟踪状态的文件，否则git会在这里列出来。最后，还显示了当前的分支，分支名是"master"，这是默认分支名。
如果这时候创建一个新的readme.txt文件，并输入一些内容，现在用`git status`命令查看状态
```bash
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        readme.txt

nothing added to commit but untracked files present (use "git add" to track)
```
可以看到新建的readme.txt文件出现在Untracked files下面。如果要查看具体修改的内容可以用`git diff`命令，后面跟要查看的文件名。

# 远程仓库
## 关联远程服务器
如果你有github账号，那么你可以把本地仓库和github仓库之间关联起来，将本地仓库放在github上，以便在其他电脑上也可以使用。github仓库和本地仓库之间的传输是使用SSH加密的，所以需要先进行配置
1. 创建SSH Key，在用户Home下，查看是否有.ssh文件夹，如果有，再看看.ssh文件夹内有没有id_rsa和id_rsa.pub两个文件，如果都有了那么可以直接进入下一步。如果没有需要创建SSH Key，打开shell（windows下打开git bash）输入以下命令，把邮件地址换成你自己的邮件地址，然后一路回车即可。id_rsa是生成的私钥，id_rsa.pub是公钥，私钥不能泄露出去。:
```bash
$ ssh-keygen -t rsa -C "youremail@example.com"
```
2. 打开github，进入Settings，选择左边的SSH and GPG keys，点击New SSH key，随意起个名字，然后在key文本框里粘贴id_rsa.pub的内容，最后点击Add key就行了。

为什么GitHub需要SSH Key呢？因为GitHub需要识别出你推送的提交确实是你推送的，而不是别人冒充的，而Git支持SSH协议，所以，GitHub只要知道了你的公钥，就可以确认只有你自己才能推送。

当然，GitHub允许你添加多个Key。假定你有若干电脑，你一会儿在公司提交，一会儿在家里提交，只要把每台电脑的Key都添加到GitHub，就可以在每台电脑上往GitHub推送了。

最后，在GitHub上免费托管的Git仓库，任何人都可以看到（但只有你自己才能改）。所以，不要把敏感信息放进去。

## 添加本地仓库到远程仓库
在github上创建一个新的仓库，创建好后仓库是空的，然后在本地仓库下运行命令：
```bash
git remote add origin git@github.com:qrs101/abc.git
```
关联完成后，可以把本地库的内容推送到远程仓库，用`git push`命令，由于远程仓库是空的，所以第一次推送时需要加上`-u`参数
```bash
git push -u origin master
```
表示把master分支推送到远程仓库上，然后把本地的master分支和远程的master分支关联起来，在以后推送和拉取时可以简化命令，以后的推送就可以不使用`-u`参数。

## 从服务器克隆git仓库
如果你想要获取一份已经存在的git仓库的拷贝，比如从github上拷贝一个仓库，这时候就要用到`git clone`命令。克隆仓库的命令格式是`git clone [url]`。git支持多种数据传输协议，常用的有https协议和SSH协议。例如使用https协议从我的github上克隆一个仓库，输入如下命令
```Bash
$ git clone https://github.com/qrs101/LeetCode.git
```
使用SSH协议克隆一个仓库如下
```bash
$ git clone git@github.com:qrs101/LeetCode.git
```
这会在当前目录下创建一个名为LeetCode的文件夹，并在这个文件夹下初始化一个.git文件夹，从远程仓库拉取的所有数据放入LeetCode文件夹下，进入LeetCode文件夹下，你会发现所有的项目文件已经在里面了。如果需要在克隆远程仓库的时候自定义本地仓库名字，可以使用如下命令
```bash
$ git clone git@github.com:qrs101/LeetCode.git abc
```
这将执行相同的操作，只不过本地创建的仓库名字为abc。当然，第一次使用git的`clone`或者`push`连接github时，会得到一个警告，按照提示输入`yes`就行了。
当远程仓库上做了修改，本地暂时还没有修改时，可以使用`git pull`命令将远程仓库上的修改拉取到本地。